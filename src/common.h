#ifndef COMMON_H
#define COMMON_H

#include <cstdint>

using Baseunit = uint32_t;

struct Rectangle
{
  Baseunit width;
  Baseunit height;
};

struct Position
{
  Baseunit x;
  Baseunit y;
};

#endif /*COMMON_H */
