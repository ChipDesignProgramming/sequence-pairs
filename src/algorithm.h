#ifndef ALGORITHM_H
#define ALGORITHM_H

#include <vector>
#include "common.h"

namespace Algorithm {

struct Packing
{
  Rectangle chip_area;
  std::vector<Position> positions;
};

Packing compute_minimal_packing(std::vector<Rectangle> const& rectangles);
}

#endif /*ALGORITHM_H */
