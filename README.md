Summary
-------
This repository contains setup and example instances for an exercise of implementing a minimal packing algorithm based on sequence pairs.

Compilation
-------
If `cmake` is available, you can compile the program using the following command in the root directory of the project.
```
cmake . && make minimal-packing
```

Usage
-----
```
./minimal-packing instance_file
```

Formatting
----------
If you do not want to worry about formatting at all, you might want to use an automatic formatting tool, for example `clang-format`.
A possible `.clang-format` is provided (change it if you want).
Usage (if it is not integrated into your editor/IDE anyway) for all files is
```
clang-format -i src/*
```

Static analysis
---------------
Static analysis can give you useful advice on improvements on your code.
For example, you can use `clang-tidy` with the provided `.clang-tidy` (which gives quite a lot of hints, you can also modify this).
First, you have to setup a compilation database using `cmake`:
```
cmake . -DCMAKE_EXPORT_COMPILE_COMMANDS=1
```

Then, usage for all files is
```
clang-tidy src/*
```

Optimum solution for instances
------------------------------
The optimum solution for the instances are\
2: 10 x 27\
3: 12 x 19\
4: 23 x 25\
5: 15 x 22\
6: 28 x 21\
7: 23871670 x 21321340
